#!/usr/bin/env python

"""
Intended to be run as a cronjob and alert of
new upstream versions, RC bugs, removal times
of packages by querying UDD.
"""

smtp_server = "localhost"

def udd_connect():
    """
    Connect to UDD.
    @params: none
    @returns: db cursor
    """
    import psycopg2
    conn = psycopg2.connect("service=udd")
    cursor = conn.cursor()
    return cursor

def get_packages(maintainer_email):
    cursor = udd_connect()
    cursor.execute("SELECT DISTINCT source FROM packages_summary WHERE maintainer_email='%s' ORDER BY source" % maintainer_email)
    packages = cursor.fetchall()
    return packages

def get_upstream_status(package):
    cursor = udd_connect()
    cursor.execute("SELECT source, status FROM upstream WHERE source='%s'" % package)
    for item in cursor.fetchall():
        if item is not None and item[1] != None and item[1] != 'up to date':
            return [{'source': item[0], 'status': 'Upstream: ' + str(item[1])}]

def get_autoremoval(package):
    import time
    cursor = udd_connect()
    cursor.execute("SELECT source, removal_time, bugs FROM testing_autoremovals WHERE source='%s' AND removal_time IS NOT NULL" % package)
    for item in cursor.fetchall():
        if item is not None:
            removal_time = str(time.strftime('%Y-%m-%d', time.localtime(item[1])))
            return [{'source': item[0], 'status': 'Removal time: ' + removal_time +', Bug: #'+  item[2]}]

def get_rc_bugs(package):
    cursor = udd_connect()
    cursor.execute("SELECT source, rc_bugs FROM bugs_count WHERE source='%s'" % package)
    for item in cursor.fetchall():
        if item is not None and item[1] != 0:
            return [{'source': item[0], 'status': 'RC bug count: ' + str(item[1])}]

def send_notification(subject, sender, receiver, todolist, maintainer_email):
    notification_subject = subject
    notification_msg = "TODO for %s\n\n" % (maintainer_email)
    for item in todolist:
        notification_msg += "'%s' '%s'\n" % (item['source'], item['status'])
    notification_msg += "\nSee https://udd.debian.org/dmd/?email1=%s for more." % (maintainer_email)

#   print(sender, receiver, notification_subject, notification_msg)
    send_mail(sender, receiver, notification_subject, notification_msg)

def send_mail(sender, receiver, subject, text):
    """
    Send an email.
    @params: str(sender) = email address
            str(receiver) = email address
            str(subject) = email body
            str(text) = email test
    @returns: void
    """
    # Import smtplib for the actual sending function and mail modules
    import smtplib
    from email.mime.text import MIMEText
    global smtp_server
    if not smtp_server:
        smtp_server = "localhost"

    # Create message
    msg = MIMEText(text)
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = receiver

    # Send message
    smtp_mail = smtplib.SMTP(smtp_server)
    smtp_mail.sendmail(receiver, [sender], msg.as_string())
    smtp_mail.quit()

def main():
    sender = "ulrike@debian.org"
    receiver = "ulrike@debian.org"
    subject = "Debian/Tails: todo reminder"
    maintainer_email = "ulrike@debian.org"
    # packages = get_packages(maintainer_email)
    packages = ['bilibop', 'keyringer', 'libgsecuredelete', 'libotr', 'mat', 'nautilus-wipe', 'onionshare', 'onioncircuits', 'pidgin-otr', 'seahorse-nautilus', 'tails-installer', 'torbirdy', 'torsocks'];
    todolist = []

    # make todolist
    for package in packages:
        if(get_upstream_status(package)):
            todolist.extend(get_upstream_status(package))
        if(get_rc_bugs(package)):
            todolist.extend(get_rc_bugs(package))
        if(get_autoremoval(package)):
            todolist.extend(get_autoremoval(package))

    # send todolist
    send_notification(subject, sender, receiver, todolist, maintainer_email)

main()
